﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	//Variable that reference to the component that handles movement;
	public CharacterController controller;
	
	//Variable to control the speed of the movements
	public float speed = 12f;

    // Update is called once per frame
    void Update()
    {	
		
		//Getting movement inputs
        float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("VerticalMov");
		float z = Input.GetAxis("Vertical");
		
		//Creating a vector pointed in the direction of the movement vanted, based on the player camera direction.
		Vector3 move = transform.right * x + transform.up * y + transform.forward * z;
		//Move the character regulating speed and preventing problems due to framerate.
		controller.Move(move * speed * Time.deltaTime);
    }
}
