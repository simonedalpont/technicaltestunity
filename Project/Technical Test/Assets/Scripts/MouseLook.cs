﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
	//Used to control mouse speed.
	public int mouseSensitivity = 100;
	
	//Variable that reference to the object that will be rotated;
	public Transform playerBody;
	
	float xRotation = 0f;
	
    // Start is called before the first frame update
    void Start()
    {
		//Hide the cursor from the screen while playing.
		Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
		//getting mouse coordinates. Time.deltaTime used to prevent a different speed rotation based on framerate.
		float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
		float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
		
		//Calculate the value to apply to the x axis for rotation.
		xRotation -= mouseY;
		//Prevents the camera to rotate over or below a certain angle.
		xRotation = Mathf.Clamp(xRotation, -90f, 90f);
		//Vertical rotation of the view.
		transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
		
		//Allows horizontal mouse looking.
		playerBody.Rotate(Vector3.up * mouseX);
    }
}
