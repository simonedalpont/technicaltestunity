﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Behavior : MonoBehaviour
{
    Vector3 agentPos;

    // Update is called once per frame
    void Update()
    {
		//Retrieving Agent position
        agentPos = GameObject.Find("Relevant Agent").transform.position;
		//Calculating the distance between A.I. and Agent
		float distance = Distance(transform.position,agentPos);//Mathf.Sqrt( (Mathf.Pow(transform.position.x - agentPos.x,2)) + (Mathf.Pow(transform.position.y - agentPos.y,2)) + (Mathf.Pow(transform.position.z - agentPos.z,2)) );
		
		//Setting different messages based on the calculated distance value
		if(distance<5) Debug.Log("Threat Zone");
		else if (5<=distance && distance<10) Debug.Log("Danger Zone");
		else if (10<=distance && distance<15) Debug.Log("Alert Zone");
		else if (15<=distance) Debug.Log("Safe Zone");
    }
	
	//Calculate the distance between the positions of two objects
	float Distance(Vector3 posObj1, Vector3 posObj2){
		return Mathf.Sqrt( (Mathf.Pow(posObj1.x - posObj2.x,2)) + (Mathf.Pow(posObj1.y - posObj2.y,2)) + (Mathf.Pow(posObj1.z - posObj2.z,2)) );
	}
}
